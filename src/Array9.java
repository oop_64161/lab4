import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

        Scanner sc = new Scanner(System.in);
        System.out.println();
        System.out.print("please input search value: ");
        int SearchValue = sc.nextInt();
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (SearchValue == arr[i]) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            System.out.print("found at index: " + index);
        } else {
            System.out.print("not found");
        }

    }
}
