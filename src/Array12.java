import java.util.Scanner;

public class Array12 {

    static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    static void printWelcome() {
        System.out.println("Welcome to my app!!!");
    }

    static int inputChoice() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input your choice(1-3): ");
            int choice = sc.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.println("Error: Please input between 1-3");
        }
    }

    static void exitProgram() {
        System.out.println("Bye!!");
        System.exit(0);
    }

    static void HelloWorldNTime() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time: ");
        int time = sc.nextInt();
        for (int i = 0; i < time; i++) {
            System.out.println("Hello World!!!");
        }
    }

    static int add(int first, int second) {
        int result = first + second;
        return result;
    }

    static void addTwonumber() {
        int result;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number: ");
        int first = sc.nextInt();
        System.out.print("Please input second number: ");
        int second = sc.nextInt();
        result = add(first, second);
        System.out.println("Result = " + result);

    }

    public static void main(String[] args) {
        int choice = 0;
        while (true) {
            printWelcome();
            printMenu();
            choice = inputChoice();
            process(choice);
        }
    }

    static void process(int choice) {
        switch (choice) {
            case 1:
                HelloWorldNTime();
                break;
            case 2:
                addTwonumber();
                break;
            case 3:
                exitProgram();
                break;
        }
    }
}
