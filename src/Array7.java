import java.util.Scanner;

public class Array7 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        int sum = 0;
        System.out.print("sum = ");
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        System.out.print(sum);
        System.out.println();

        double avg = 0;
        avg = ((double) (sum)) / arr.length;
        System.out.print("avg = " + avg);

        System.out.println();
        System.out.print("min = ");
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[index] > arr[i]) {
                index = i;
            }
        }
        System.out.print(arr[index]);
    }
}
