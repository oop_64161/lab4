import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        int ElementNum = sc.nextInt();
        int arr[] = new int[ElementNum];
        int UniqeSize = 0;

        for (int i = 0; i < arr.length; i++) {
            System.out.print("Element " + i + ": ");
            int temp = sc.nextInt();
            int index = -1;
            for (int j = 0; j < UniqeSize; j++) {
                if (arr[j] == temp) {
                    index = j;
                }
            }
            if (index < 0) {
                arr[UniqeSize] = temp;
                UniqeSize++;
            }
        }
        System.out.print("All number: ");
        for (int i = 0; i < UniqeSize; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
