import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        int sum = 0;
        System.out.print("sum = ");
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        System.out.print(sum);
        System.out.println();

        double avg = 0;
        avg = ((double) (sum)) / arr.length;
        System.out.print("avg = " + avg);

        System.out.println();
        System.out.print("min = ");
        int indexMin = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[indexMin] > arr[i]) {
                indexMin = i;
            }
        }
        System.out.print(arr[indexMin]);

        System.out.println();
        System.out.print("max = ");
        int indexMax = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[indexMax] < arr[i]) {
                indexMax = i;
            }
        }
        System.out.print(arr[indexMax]);
    }
}
